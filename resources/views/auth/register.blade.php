<!DOCTYPE html>
<html>
<head>
    <title>Sign Up</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css">
     <link rel="stylesheet" href="../css/signin.css">
</head>
<body>

    <div class="container" >

        <div class="row">

            <div class="col-md-6 col-md-offset-3">

                <div class="panel panel-login">

                    <div class="panel-body">

                        <div class="row">

                            <div class="col-lg-12" >

                                {!! Form::open(['url' => 'auth/register']) !!}

                                <div class="form-group">
                                    {!! Form::label('username', 'User Name:') !!}
                                    {!! Form::text('username', null, [ 'class' => 'form-control'] ) !!}
                                </div>

                                <div class="form-group">

                                    {!! Form::label('email', 'Email:') !!}
                                    {!! Form::email('email', null, ['class' => 'form-control']) !!}

                                </div>


                                <div class="form-group">

                                    {!! Form::label('password', 'Password:') !!}
                                    {!! Form::password('password', null, ['class' => 'form-control']) !!}

                                </div>

                                <div class="form-group">

                                    {!! Form::label('password_confirmation', 'Confirm Password:') !!}
                                    {!! Form::password('password_confirmation', null, ['class' => 'form-control']) !!}

                                </div>

        
                                <div class="form-group">

                                    {!! Form::submit('Register', ['class' => 'btn btn-primary form-control']) !!}

                                </div>

                                {!! Form::close() !!}

                                @include('errors.list')

        

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>
       

</body>
</html>