<!DOCTYPE html>
<html>
	<head>
		<title>Document</title>
			<link rel="stylesheet" href="../css/bootstrap.min.css">
			<link rel="stylesheet" href="../css/signin.css">
			 <script href="../js/jquery.js"></script>
			 <script href="../js/jquery.validate.js"></script>
			<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->

	</head>
	<body>
		<div class="container">
    	<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-login">
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-6">
								<a href="#"  id="login-form-link"></a>
							</div>
							<div class="col-xs-6">
								<a href="#" id="register-form-link" class="active" ></a>
							</div>
						</div>
						<hr>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<form id="signup-form" action="signup" method="post" role="form" style="display: block;">
									<div class="form-group">
										<input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="">
									</div>
									<div class="form-group">
										<input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="">
									</div>
									<div class="form-group">
										<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password">
									</div>
									<div class="form-group">
										<input type="password" name="confirm_password" id="confirm_password" tabindex="2" class="form-control" placeholder="Confirm Password">
									</div>
									<div>
										<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
												<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now">
											</div>
										</div>
									</div>
								</form>

								<script>

								$().ready(function() {
									alert('fdsfsdf');
		
											// validate signup form on keyup and submit
											$("#signup-form").validate({
												rules: {
													firstname: "required",
													lastname: "required",
													username: {
														required: true,
														minlength: 2
													},
													password: {
														required: true,
														minlength: 5
													},
													confirm_password: {
														required: true,
														minlength: 5,
														equalTo: "#password"
													},
													email: {
														required: true,
														email: true
													}
													
												},
												messages: {
													firstname: "Please enter your firstname",
													lastname: "Please enter your lastname",
													username: {
														required: "Please enter a username",
														minlength: "Your username must consist of at least 2 characters"
													},
													password: {
														required: "Please provide a password",
														minlength: "Your password must be at least 5 characters long"
													},
													confirm_password: {
														required: "Please provide a password",
														minlength: "Your password must be at least 5 characters long",
														equalTo: "Please enter the same password as above"
													},
													email: "Please enter a valid email address",
												}
											});
								}
								</script
	                            </div> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</body>
</html>