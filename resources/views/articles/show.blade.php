@extends('app')


@section('content')

<hr><hr>
<div class="block" padding:20px>
<div class="row">

	<!-- Blog Entries Column -->
	<div class="col-md-12">
        
		<!-- Show Article Post -->
		<div class="title">
			<a href="{{ url('/articles',$article->id) }}">{{ $article->title }}</a>
		</div>
		<p class="postedBy" align="right"><span class="glyphicon glyphicon-time">

		</span> Posted on {{ $article->published_at }}
			by <a href="#" class="postedBy" >{{ $article->username }}</a>

		</p>
		<hr>
		<p class="body">{{ $article->body }}</p>
		<hr>

		
			@unless ($article->tags->isEmpty())
		
				<h5>Tags:</h5>
				@foreach($article->tags as $tag)

				 <li> {{ $tag->name }} </li>

				@endforeach
			 
			@endunless

		</div>
		
		

	</div>

</div>


@stop
