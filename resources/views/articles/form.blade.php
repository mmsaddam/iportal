

	<div class="form-group">
		{!! Form::label('title', 'Title:') !!}
		{!! Form::text('title', null, ['class' => 'form-control']) !!}

	</div>

	<!-- Body form input -->

	<div class="form-group">

		{!! Form::label('body', 'Body:') !!}
		{!! Form::textarea('body', null, ['class' => 'form-control']) !!}

	</div>

	<!-- Published_at  Form input -->

	<div class="form-group">

		{!! Form::label('published_at', 'Published On:') !!}
		{!! Form::input('date', 'published_at',date('Y-m-d'), ['class' => 'form-control']) !!}

	</div>

   <!-- tags from input -->

   <div class="form-group">

   		{!! Form::label('tag_list',' Tags:') !!}
   		{!! Form::select('tag_list[]',$tags, null , ['id' => 'tag_list', 'class' => 'form-control', 'multiple'] ) !!}

   </div>
	
	<!-- Add Article form input -->
	<div class="form-group">

		{!! Form::submit($submitButtonText, ['id' => 'submitBtn', 'class' => 'btn btn-primary form-control']) !!}

	</div>

	@section('footer')

		<script>

		

          $('#tag_list').select2({

          		placeholder: 'Select a tag',
          		
  
			});

          

		</script>

	@endsection