@extends('app')


@section('content')

<hr> <hr>
 <!-- Page Content -->
    
        <div class="row">

            <!-- Blog Entries Column -->
            <div class="col-md-8">

                @foreach ($articles as $article)
                <div class="block">
                
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu4">
                  <li><a href="#">Regular link</a></li>
                  <li class="disabled"><a href="#">Disabled link</a></li>
                  <li><a href="#">Another link</a></li>
                </ul>

                <!-- Show Article Post -->
                    <div class="title">
                        <a href="{{ url('/articles',$article->id) }}">{{ $article->title }}</a> 
                    </div>

                     <!-- show posted by -->
                        <p class="postedBy" align="right">
                            <span class="glyphicon glyphicon-time"></span> Posted on {{ $article->published_at }} by <a href="#" class="postedBy" > {{ $article->username }} </a> 
                        </p>
                
                     <!-- show article body -->
                    <p class="body">{{ $article->body }}</p>
                    
                    <div class="row">

                        <div class="col-md-8">

                            <a class="btn btn-primary" href="{{ url('/articles',$article->id) }}">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>

                        </div>
                        
                        @unless($article->user()->id = Auth::user()->id)

                            <div class="col-md-4">

                                <p  align="right" class="editLink">

                                    <a  href="articles/{{$article->id}}/edit">Edit </a> 

                                </p>

                            </div>

                        @endunless

                    </div>
                    
                </div>
                @endforeach 

                
                <!-- Pager -->
                <ul class="pager">
                    <li class="previous">
                        <a href="#">&larr; Older</a>
                    </li>
                    <li class="next">
                        <a href="#">Newer &rarr;</a>
                    </li>
                </ul>
               
            </div>
            <hr>
             <!-- Add side widget -->
                @include('widget')
	     </div>
   
   <script>
    $(document).ready(function(){
        
        // $(".lead").mouseover(function(){
            
        //     alert("Value: " + $(".lead").text());
        // });
    
    });
    </script>
@stop















