@extends('app')

@section('content')
			 
<hr>
<h1 class="page-header">
 Write New Article
</h1>

{!!	Form::open(['url' => 'articles']) !!}

	@include('articles.form',['submitButtonText' => 'Add Article'])

{!!	Form::close() !!}
@include('errors.list')

@stop