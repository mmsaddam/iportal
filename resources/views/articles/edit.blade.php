@extends('app')


@section('content')

        <hr><hr>
		{!!	Form::Model($article,['method' => 'PUT', 'url' => ['articles',$article->id]]) !!}

			@include('articles.form',['submitButtonText' => 'Update Article'])

		{!!	Form::close() !!} 

	@include('errors.list')

@stop