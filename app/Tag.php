<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
	

	/**
	*Fillable field Tag Model
	*
	*@var Array
	*/
	   protected $fillable = [
	      	'name'
	    ];

	/**
	* Get the articles associated with given tag
	*
	*
	*
	*******/
    public function articles(){
    	return $this->belongsToMany('App\Article');
    }


}
