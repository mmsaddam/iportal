<?php

namespace App;
use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
   

/**
*Fillable field Article Model
*
*@return Array
*/
   protected $fillable = [
    'title',
    'body',
    'published_at',
    'user_id'
    ];

/**
*Database Table Used by this model
*
*@var string
*/

 protected $table = 'articles';



/*
* Query scope for the acticles that have been published
*
*@param $query
**/

public function scopePublished($query)
    {
    	
        $query->where('published_at', '<=', Carbon::now());

    }

 /*
* Query scope for the acticles that have been unpublished
*
*@param $query
**/

public function scopeUnPublished($query)
    {
    	
        $query->where('published_at', '>', Carbon::now());

    }  

/*
* Set published_at date format
*
*@param $date
*/
public function setPublishedAtAttribute($date){

	$this->attributes['published_at'] = Carbon::parse($date);

}


/*
* An article is owned by a user
*
*
*
*
***/

public function user(){

    return $this->belongsTo('App\User');
}

/*
* Get the tags associated with given article
*
*
*
*
**/

public function tags(){

    return $this->belongsToMany('App\Tag')->withTimestamps();
}



/*
* 
*Get a list of tag ids associated with the current article
*
*@return array
*
*
**/

public function getTagListAttribute(){

    return $this->tags->lists('id')->toArray();

}




}
