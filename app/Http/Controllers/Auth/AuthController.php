<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Requests;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Auth;


class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;


    protected $redirectPath = '/home';
    protected $loginPath = 'auth/login';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }


    /*Entry point of the application
    *
    *Check the user is logged in
    *
    *If login then redirect to home page
    *
    *Else redirect to login page
    *
    */

    public function index(){

        if (Auth::check()) { 

            return redirect()->intended($this->redirectPath);
        
        }else{ 

            return redirect()->intended($this->loginPath);
          
        }


    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }



    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */

    protected function create(array $data)
    {
        return User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }



    /*
    * Show login form
    *
    *
    **/

    public function getLogin(){

       return view('auth.login');

    }



    /*
    *Submit login form
    *
    *
    *
    **/
    public function postLogin(Request $request){
        
        $this->validate($request,[

            'email' => 'required',
            'password' => 'required'

            ]);

        $input = $request->all();  
        $email = $input['email'];
        $password = $input['password'];
        $remember = 0;

        if (isset($input['remember'])) {

            $remember = 1;

        }else{

            $remember = 0;

        }

        
        $cardinals = $request->only('email', 'password');


        if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {

            return redirect($this->redirectPath);

        }else{

            return  redirect($this->loginPath)->withErrors('Cardinal missmatch');
            
        }

        
    }




    /*
    * Show register form
    *
    *
    **/
    public function getRegister(){

         return view('auth.register');

    }



    /*
    * Submit register form
    *
    *
    **/
    public function postRegister(Request $request){

        $this->validate($request,[

            'username' => 'required|max:255',

            'email' => 'required|email|max:255|unique:users',

            'password' => 'required|confirmed|min:6',

            ]);

        $cardinals = $request->only('username', 'email', 'password');

        

        $user = $this->create($cardinals);

        if ($user) {

            Auth::login($user);

            return redirect($this->redirectPath);

        }else{

            var_dump('Ops! fail registration');
        }

       // Auth::login($user);

        

    }


    /*
    *User get logut
    *
    *@redirect to Login page
    *
    */

    public function getLogout(){

        if (Auth::check()) {

            var_dump('logged in');

         }  else{

            var_dump('logout');

         }      

         Auth::logout();
         return  redirect($this->loginPath);

    }




}
