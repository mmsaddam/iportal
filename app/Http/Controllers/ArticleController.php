<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ArticleRequest;
use App\Article;
use App\User;
use Carbon\Carbon;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Session;
use App\Tag;

class ArticleController extends Controller
{
   

  /**
   * create a new articles controller instance
   */

   public function __construct()
    {
        $this->middleware('auth', ['except' => ['index', 'show']]);
    }

    /**
     * Display al articles
     *
     * @return Response
     */

    public function index(Request $request)
    {
        
        if (Auth::check()) { 

            $input = $request->all();  
            $articles = Article::latest('published_at')->published()->get(); 

            foreach ($articles as $key => $value) {

                $user = User::where('id', '=', $value->user_id)->first();

                $value['username'] = $user->username;
                $articles[$key] = $value;
             
            }

        
            return view('articles.index',compact('articles'));

            }else{  

               return redirect('auth/login'); // Redirect to login page
            }
    
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */

    public function create()
    {
        $tags = Tag::lists('name','id');
        return view('articles.create',compact('tags'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */

    public function store(ArticleRequest $request){
          
            $this->createArticle($request);
            return redirect('/home');
        
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show(Article $article)
    {
       
        if (Auth::check()) { 

            $user = User::where('id', '=', $article->user_id)->first();
            $article['username'] = $user->username;

           
            return view('articles.show',compact('article'));

        }else{  

            return redirect('auth/login'); // Redirect to login page
        }
        

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  Article $article
     * @return Response
     */
    public function edit(Article $article)
    {
         
        $tags = Tag::lists('name','id');
        return view('articles.edit',compact(['article','tags']));

    }


    
    
    /*
    *Update an Article
    *
    *Param integer $id
    *
    *@Return Response
    */

    public function update(Article $article, ArticleRequest $request){
      
        $article->update($request->all());
        $this->syncTags($article, $request->input('tag_list'));

         return redirect('/profile');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */

    public function destroy($id)
    {
        //
    }

    /**
    * Sync up the list of tags in the database
    *
    *@param ArticleRequest $request
    *
    *@param array $tags
    **/

    private function syncTags(Article $article, array $tags){

                $article->tags()->sync($tags);

    }


    /*
    * Save a  new article
    *
    *@param ArticleRequest $request
    *
    */

    private function createArticle(ArticleRequest $request){

        $article = Auth::user()->articles()->create($request->all());

        $this->syncTags($article, $request->input('tag_list'));

    }




}
