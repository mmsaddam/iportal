<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Article;
use Auth;


class UserController extends Controller
{
    
    /*
    *About Me
    *
    *
    **/

    public function about(){

        if (Auth::check()) { 

            $user = Auth::user();    

            return view('about',compact('user'));

        }else{

            return 'Please login first';
        }

        
    }

    public function index(Request $request){

    	return 'user index';

    }

    /*
    * Show the article posted by the user
    *
    *
    **/
    
    public function profile(){

        if (Auth::check()) { 

            $articles = Article::where('user_id', '=', Auth::user()->id) ->latest('published_at')->published()->get();

            $username = User::where('id', '=', Auth::user()->id)->first()->username;

            foreach ($articles as $key => $value) {

                $value['username'] = $username;
                $articles[$key] = $value;
             
            }

            return view('users.profile',compact('articles'));

            }else{  

               return redirect('auth/login'); // Redirect to login page
            }

        
    }


}
