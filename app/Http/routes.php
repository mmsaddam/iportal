<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/




Route::get('/', 'Auth\AuthController@index');

// Authentication routes...

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...

Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');


Route::get('home', [

    'middleware' => 'auth',
    'uses' => 'ArticleController@index'
]);


// // store new article
// Route::post('articles','ArticleController@store');

// Route::post('update/{id}','ArticleController@update');

// //  new article blade
// Route::get('articles/create','ArticleController@create');

// // show individual article
// Route::get('articles/{id}','ArticleController@show');
// Route::get('edit/{id}','ArticleController@edit');

// show about the user
Route::get('about','UserController@about');

// show article posted by user
Route::get('profile','UserController@profile');

Route:Resource('articles','ArticleController');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',
]);

/*
Route::get('/','UserController@index');

Route::get('signup','UserController@signup');

Route::post('signup','UserController@signupSuccess');

Route::get('articles','ArticleController@index');

Route::post('login','UserController@login');

Route::post('articles','ArticleController@store');

Route::get('articles/create','ArticleController@create');

Route::get('articles/{id}','ArticleController@show');

Route::get('about','UserController@about');

*/